package com.edd.messaging.producer;
 
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.edd.broker.producer.BasicKafkaProducerWrapper;

@Component
public class KafkaProducerWrapper extends BasicKafkaProducerWrapper {
	private static final Logger logger = LoggerFactory.getLogger(KafkaProducerWrapper.class);

	@Autowired
	private Environment env;
	
    @PostConstruct
    public void init() {
		try {
	    	Properties kafkaProps = loadProperties("kafka");
			super.init(kafkaProps);
		} catch (IOException e) {
			logger.error("Could not load the kafka configuration.", e);
		}
    }
    
    public Properties loadProperties(String fileName) throws IOException {
        final Properties prop = new Properties();
        try {
            prop.load(this.getClass().getClassLoader().getResourceAsStream(""+fileName+".properties"));
            Arrays.stream(env.getActiveProfiles()).forEach(activeProfile ->{
            	try {
                    prop.load(this.getClass().getClassLoader().getResourceAsStream(""+fileName+"-"+activeProfile+".properties"));
            	}
                catch(Exception e) {
                	logger.error(e.getMessage(), e);
                }
            });
        }
        catch(Exception e) {
        	logger.error(e.getMessage(), e);
        }
        return prop;

    }
    
}