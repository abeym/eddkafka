CREATE TABLE IF NOT EXISTS voda_user
(
	userid integer PRIMARY KEY,
	username text NOT NULL, 
	password text 
);

CREATE TABLE IF NOT EXISTS user_mi
(
	id integer PRIMARY KEY, 
	balance numeric, 
	rate_plan text, 
	consumed_data numeric, 
	sallefny numeric, 
	total_qouta numeric, 
	usb_msisdn text, 
	user_userid integer,
	CONSTRAINT fk_user FOREIGN KEY(user_userid) REFERENCES voda_user(userid)
);

