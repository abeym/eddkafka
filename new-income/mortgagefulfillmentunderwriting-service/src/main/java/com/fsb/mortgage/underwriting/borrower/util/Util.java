package com.fsb.mortgage.underwriting.borrower.util;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.edd.messages.BorrowerDto;
import com.edd.messages.IncomeDto;

@Service
public class Util {
	@Autowired
	private ModelMapper modelMapper;
	public com.fsb.mortgage.underwriting.borrower.model.Income convertToModel(IncomeDto income){
		com.fsb.mortgage.underwriting.borrower.model.Income incomeModel = modelMapper.map(income, com.fsb.mortgage.underwriting.borrower.model.Income.class);
		BorrowerDto borrower = income.getBorrower();
		com.fsb.mortgage.underwriting.borrower.model.Borrower borrowerModel;
		if(borrower!=null) {
			borrowerModel = modelMapper.map(borrower, com.fsb.mortgage.underwriting.borrower.model.Borrower.class);
			incomeModel.setBorrower(borrowerModel);
		}
		return incomeModel;
	}

	public IncomeDto convertToDTO(com.fsb.mortgage.underwriting.borrower.model.Income incomeModel){
		IncomeDto income = modelMapper.map(incomeModel, IncomeDto.class);
		com.fsb.mortgage.underwriting.borrower.model.Borrower borrowerModel = incomeModel.getBorrower();
		BorrowerDto borrower;
		if(borrowerModel!=null) {
			borrower = modelMapper.map(borrowerModel, BorrowerDto.class);
			income.setBorrower(borrower);
		}
		return income;
	}

}
