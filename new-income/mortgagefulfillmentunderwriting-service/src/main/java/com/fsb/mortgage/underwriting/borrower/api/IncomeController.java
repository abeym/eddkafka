package com.fsb.mortgage.underwriting.borrower.api;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.edd.messages.IncomeDto;
import com.fsb.mortgage.underwriting.borrower.repository.IncomeRepository;
import com.fsb.mortgage.underwriting.borrower.util.Util;

@RestController
public class IncomeController {
    @Autowired
    private IncomeRepository incomeRepository;
    
    @Autowired
    private ModelMapper modelMapper;
    
    @Autowired
    private Util util;

    @PostMapping("/income")
    public IncomeDto publishEvent(@RequestBody IncomeDto income) {
    	incomeRepository.save(util.convertToModel(income));
        return income;
    }

    @GetMapping("/income")
	public IncomeDto getIncome(@PathVariable int incomeId) {
		return util.convertToDTO(incomeRepository.findById(incomeId));
	}

    @GetMapping("/incomes")
	public List<IncomeDto> getIncomes() {
    	List<IncomeDto> incomes = new ArrayList<IncomeDto>();
    	incomeRepository.findAll().forEach(income -> incomes.add(util.convertToDTO(income) ));
		return incomes;
	}

	@PutMapping("/income")
	public IncomeDto putIncome(@RequestBody IncomeDto income) {
    	incomeRepository.save(util.convertToModel(income));
        return income;
	}

	@DeleteMapping("/income")
	public Integer deleteIncome(@PathVariable Integer incomeId) {
		incomeRepository.deleteById(incomeId);
		return incomeId;
	}


}
