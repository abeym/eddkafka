package com.fsb.mortgage.underwriting.borrower.repository;

import org.springframework.data.repository.CrudRepository;

import com.fsb.mortgage.underwriting.borrower.model.Income;


public interface IncomeRepository extends CrudRepository<Income, Integer>{
	public Income findById(int id);
}
