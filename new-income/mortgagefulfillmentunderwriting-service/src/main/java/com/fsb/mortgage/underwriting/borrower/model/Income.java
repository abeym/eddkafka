package com.fsb.mortgage.underwriting.borrower.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="income")
public class Income {

	@Id
	private Integer id;
	@ManyToOne(cascade = CascadeType.ALL, targetEntity = Borrower.class)
	@JoinColumn(name="borrower_id")
	private Borrower borrower;
	private Long baseIncome;
	private Long incomeAmount;
	private Long overtimeAmount;
	private Long bonuses;
	private Long commissions;
	private Long dividendInterest;
	private Long other;
	private Long netRental;
	private Long subjectNetCash;
	private Long total;

}
