package com.fsb.mortgage.underwriting.borrower.model;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Borrower {
	@Id
	private Integer id;
	private String name;
	private String address;
	private String ssn;
}
