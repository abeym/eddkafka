package com.fsb.mortgage.underwriting.borrower.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

import com.edd.messages.IncomeDto;
import com.fsb.mortgage.underwriting.borrower.repository.IncomeRepository;
import com.fsb.mortgage.underwriting.borrower.util.Util;

@EnableBinding(Sink.class)
public class IncomeListener {
    private Logger logger = LoggerFactory.getLogger(IncomeListener.class);

    @Autowired
    private IncomeRepository incomeRepository;
    
    @Autowired
    private Util util;
    
    @StreamListener("input")
    public void consumeMessage(IncomeDto income) {
        logger.info("Consume payload : " + income);
        incomeRepository.save(util.convertToModel(income));
    }



}
