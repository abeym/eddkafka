package com.edd.messages;

import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"id",
	"borrower",
	"baseIncome",
	"incomeAmount",
	"overtimeAmount",
	"bonuses",
	"commissions",
	"dividendInterest",
	"other",
	"netRental",
	"subjectNetCash",
	"total"
})
@Generated("jsonschema2pojo")
@Data

public class IncomeDto {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("borrower")
	private BorrowerDto borrower;
	@JsonProperty("baseIncome")
	private Long baseIncome;
	@JsonProperty("incomeAmount")
	private Long incomeAmount;
	@JsonProperty("overtimeAmount")
	private Long overtimeAmount;
	@JsonProperty("bonuses")
	private Long bonuses;
	@JsonProperty("commissions")
	private Long commissions;
	@JsonProperty("dividendInterest")
	private Long dividendInterest;
	@JsonProperty("other")
	private Long other;
	@JsonProperty("netRental")
	private Long netRental;
	@JsonProperty("subjectNetCash")
	private Long subjectNetCash;
	@JsonProperty("total")
	private Long total;

}
