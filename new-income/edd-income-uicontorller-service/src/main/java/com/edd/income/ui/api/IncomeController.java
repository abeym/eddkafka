package com.edd.income.ui.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.edd.income.ui.feign.IncomeClient;
import com.edd.messages.IncomeDto;

@EnableBinding(Source.class)
@RestController
public class IncomeController {
    @Autowired
    private MessageChannel output;
    
    @Autowired
    private IncomeClient incomeClient;

    @PostMapping("/income")
    public IncomeDto publishEvent(@RequestBody IncomeDto income) {
        output.send(MessageBuilder.withPayload(income).build());
        return income;
    }

    @GetMapping("/income/{id}")
	public IncomeDto getIncome(@PathVariable int id) {
		return incomeClient.getIncome(id);
	}

    @GetMapping("/incomes")
	public List<IncomeDto> getIncomes() {
		return incomeClient.getIncomes();
	}

    @PutMapping("/income")
	public IncomeDto putIncome(@RequestBody IncomeDto income) {
		output.send(MessageBuilder.withPayload(income).build());
		return income;
	}

	@DeleteMapping("/income")
	public Integer deleteIncome(@PathVariable Integer incomeId) {
		IncomeDto income = null;
		output.send(MessageBuilder.withPayload(income).build());
		return incomeId;
	}


}
