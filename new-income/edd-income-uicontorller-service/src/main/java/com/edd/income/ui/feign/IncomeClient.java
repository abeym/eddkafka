package com.edd.income.ui.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.edd.messages.IncomeDto;

@FeignClient(url="http://localhost:9193", name = "INCOME-BACKEND-CLIENT")
//@FeignClient(url="income_backend_url", name = "INCOME-BACKEND-CLIENT")
public interface IncomeClient {
	
	@GetMapping("/incomes")
	public List<IncomeDto> getIncomes();

	@GetMapping("/income/{id}")
	public IncomeDto getIncome(@PathVariable("id") int id);
}
