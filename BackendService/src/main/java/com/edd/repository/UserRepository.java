package com.edd.repository;

import org.springframework.data.repository.CrudRepository;

import com.edd.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
	public User findOneByMsisdn(String msisdn);
}
