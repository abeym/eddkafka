package com.edd.service;

import java.util.Optional;

import com.edd.model.MobileInternet;
import com.edd.model.User;

public interface UserService {
	public User getUserData(String user, String password);
	
	public Optional<MobileInternet> getUserMi(String msisdn);
	
}
