package com.edd.feign.client;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import com.edd.feign.dto.User;

@FeignClient(url="http://jsonplaceholder.typicode.com", name = "USER-CLIENT")
public interface UserClient {
	
	@GetMapping("/users")
	public List<User> getUsers();
}
