package com.edd.feign.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.edd.feign.client.UserClient;
import com.edd.feign.dto.User;

@RestController
public class UserController {
	
	@Autowired
	private UserClient userClient;

	@GetMapping("/findAllUsers")
	public List<User> getAllUsers(){
		return userClient.getUsers();
	}
}
